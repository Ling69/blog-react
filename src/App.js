
import { Layout } from 'antd';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import { Nav } from './components/Nav';
import { Login } from './pages/Login';
import { Register } from './pages/Registrer';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { Home } from './pages/Home';
import { ProtectedRoute } from './components/ProtectedRoute';
import { loginWithToken } from './stores/auth-slice';
import { NewPost } from './pages/NewPost';
import { Users } from './pages/Users';
import { PersonalPage } from './components/PersonalPage'


const { Header, Content, Footer } = Layout;

function App() {

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loginWithToken());
  }, [dispatch]);

  return (
    <Layout>
      <Header style={{ background: '#fff' }}>
        <Nav />
      </Header>
      <Content>
        <Switch>
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <ProtectedRoute path="/newpost">
            <NewPost />
          </ProtectedRoute>
          <ProtectedRoute path="/users">
            <Users />
          </ProtectedRoute>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/post/:id" children={<PersonalPage />} />

        </Switch>
      </Content>
      <Footer>
        <p>copyright@simplon</p>
      </Footer>
    </Layout>

  );
}

export default App;
