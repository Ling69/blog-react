import { Card } from "antd";
import { PostList } from "../components/PostList";


export function Home() {

    return(
        <Card>
            <PostList />
        </Card>

    )
}