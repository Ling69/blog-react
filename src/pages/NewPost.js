import { Card, Divider } from "antd";
import { AddPost } from "../components/AddPost";


export function NewPost () {
    

    return(

        <Card>
             <Divider>
            <h1>Add Post</h1>
            </Divider>
            <AddPost />
        </Card>
          
    )
}