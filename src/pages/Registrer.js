import { Card, Divider } from "antd";
import { UserForm } from "../components/UserForm";

export function Register() {
    
    return (
        <Card>
            <Divider>
            <h2>Register</h2>
            </Divider>
            <UserForm />
        </Card>
    )
}