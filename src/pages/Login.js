import { Card, Divider } from "antd";
import { LoginForm } from "../components/LoginForm";

export function Login() {

    return(
        <Card>
            <Divider>
            <h2>Login</h2>
            </Divider>
            <LoginForm />
        </Card>
    )
}