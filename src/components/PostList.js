import { Col, Row } from "antd";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { fetchPosts } from "../stores/post-slice";
import { Post } from "./Post";


export function PostList() {

    const dispatch = useDispatch();
    const post = useSelector(state => state.post.list);

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);

    return (

        <Row onClick="submit" gutter={[30,30]}>
            {post.map(item =>
                    <Col key={item.id} span={12}>
                <Link to={'/post/'+item.id}>
                        <Post post={item} />
                </Link>
                    </Col>
            )}

        </Row>

    )
}