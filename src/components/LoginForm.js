import { Button, Form, Input } from 'antd';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from 'react-router-dom';
import { loginWithCredentials } from '../stores/auth-slice';

export function LoginForm() {
    
    const dispatch = useDispatch();
    const feedback = useSelector(state => state.auth.loginFeedback);
    
    //this fuction to help us to the homepage when we login 
    let history = useHistory();
    function handleClick() {
        history.push("/");
      }

    const onFinish = (values) => {
        dispatch(loginWithCredentials(values));
        handleClick();
    }
    
    return(
        <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 10,
            }}
            onFinish={onFinish}
        >
            {feedback && <p>{feedback}</p>}

            <Form.Item
                label="Username"
                name="name"
                rules={[
                    {
                        required: true,
                        message: 'Username is required',
                    },
                  
                ]}
            >
                <Input  />
            </Form.Item>

            <Form.Item
                label="Email"
                name="email"
                rules={[
                    {
                        required: true,
                        message: 'Email is required',
                    },
                    {
                        type: 'email',
                        message: 'Please enter a valid email',
                    },
                ]}
            >
                <Input type="email" />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[
                    {
                        required: true,
                        message: 'Password is required',
                    },
                ]}
            >
                <Input.Password />
            </Form.Item>



            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button type="primary" htmlType="submit">
                    Login
                </Button>
            </Form.Item>
        </Form>
        
    )
}
