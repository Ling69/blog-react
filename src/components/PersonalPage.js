import { Card } from "antd";
import Meta from "antd/lib/card/Meta";
import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Redirect, useParams } from "react-router-dom";
import { deletePost } from "../stores/post-slice";
import { DeleteOutlined } from "@ant-design/icons";


export function PersonalPage() {

    const { id } = useParams()

    const dispatch = useDispatch()

    const [post, setPost] = useState(null)

    const [redirect, setRedirect] = useState(false)


    useEffect(() => {
        async function fetchPost() {
            const response = await axios.get(process.env.REACT_APP_SERVER_URL + '/api/post/' + id)
            setPost(response.data);
            }; 
        fetchPost();
        }, [id]);

    async function deleteThis(){

        dispatch(deletePost(post.id))
            setRedirect(true)}

    return (
        <>
            {post &&
                <div>
                    {redirect ? <Redirect push to='/'/> : null}
                    <Card
                        hoverable
                        style={{ width: 500 }}
                        cover={<img src={post.image.startsWith('http')? post.image:process.env.REACT_APP_SERVER_URL+post.image} alt="" />}
                    >
                        <Meta title={post.title} description={post.content} />
                        <br/>
                        <p>Author:{post.author}</p>
                        {/* <p>UserId:{post.userId}</p> */}
                        <p>{post.released} </p>
                        <p>Commentaire: <br />{post.commentaire}</p>
                        <button onClick={()=>deleteThis()}><DeleteOutlined /></button>
                    </Card>,

                </div>
            }
        </>

    );
}

