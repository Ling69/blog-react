import { Card } from "antd";

export function UserId ({user}) {

    return(
        <Card
        hoverable
        cover={<img alt={user.title} src={user.image} />}
    >
        <Card.Meta title={user.title} description={user.content} />
    </Card>
    );
}