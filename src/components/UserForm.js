import { Form, Button, Input } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { register } from "../stores/auth-slice";


export function UserForm() {

    const dispatch = useDispatch();
    const feedback = useSelector(state => state.auth.registerFeedback);

    const onFinish = (values) => {
        dispatch(register(values))
    }
    return(
        <Form
        name="basic"
        labelCol={{
            span: 8,
        }}
        wrapperCol={{
            span: 10,
        }}
        onFinish={onFinish}
    >
        {feedback && <p>{feedback}</p>}
        <Form.Item
                label="Username"
                name="username"
                rules={[
                    {
                        required: true,
                        message: 'Username is required',
                    },
                    
                ]}
            >
                <Input type="username" />
            </Form.Item>

        <Form.Item
            label="Email"
            name="email"
            rules={[
                {
                    required: true,
                    message: 'Email is required',
                },
                {
                    type: 'email',
                    message: 'Please enter a valid email',
                },
            ]}
        >
            <Input type="email" />
        </Form.Item>

        <Form.Item
            label="Password"
            name="password"
            rules={[
                {
                    required: true,
                    message: 'Password is required',
                },
            ]}
        >
            <Input.Password />
        </Form.Item>



        <Form.Item
            wrapperCol={{
                offset: 8,
                span: 16,
            }}
        >
            <Button type="primary" htmlType="submit">
                Register
            </Button>
        </Form.Item>
    </Form>
    )
        }