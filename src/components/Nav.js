import { Menu } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import { logout } from "../stores/auth-slice";

export function Nav() {

    const location = useLocation();

    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.user);

    return(
        <Menu mode="horizontal" theme="light" selectedKeys={[location.pathname]}>
        <Menu.Item key="/">
            <Link to="/">Home
            </Link>
        </Menu.Item>
        {!user ? <>
            <Menu.Item key="/register">
                <Link to="/register">Register</Link>
            </Menu.Item>

            <Menu.Item key="/login">
                <Link to="/login">Login</Link>
            </Menu.Item>
        </>
            :
            <>
                <Menu.Item key="/newpost">
                    <Link to="/newpost">AddPost</Link>
                </Menu.Item>
                
                <Menu.Item key="/logout" onClick={() => dispatch(logout())}>
                    Logout
                </Menu.Item>
            </>}
    </Menu>


    )
}