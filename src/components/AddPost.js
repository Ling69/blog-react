import { Button, Form, Input, Upload } from "antd";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { uploadPost } from "../stores/post-slice";


export const AddPost = () => {

    const [picture, setPicture] = useState(null);
    let user = useSelector(state => state.auth.user)
    const dispatch = useDispatch();

    let history = useHistory();
    function handleClick() {
        history.push("/");
    }

    const handleFile = (file) => {
        setPicture(file);
        return false;
    }

    const onFinish = (values) => {
        dispatch(uploadPost({ ...values, picture, userId: user.id }));
        handleClick();

    }

    return (
        <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 10,
            }}
            onFinish={onFinish}
        >

            <Form.Item
                label="Title"
                name="title"
                rules={[

                    {
                        required: true,
                        message: 'Please add the title',
                    },
                ]}
            >
                <Input type="text" />
            </Form.Item>

            <Form.Item
                label="Content"
                name="content"
                rules={[

                    {
                        required: true,
                        message: 'please enter your content',
                    },
                ]}
            >
                <Input type="text" />

            </Form.Item>

            <Form.Item
                label="Image"
                name="picture"
                rules={[

                    {
                        required: true,
                        message: '',
                    },
                ]}
            >
                <Upload beforeUpload={handleFile} fileList={[]}>
                    <Button>Select File</Button>
                </Upload>

            </Form.Item>

            <Form.Item
                label="Author"
                name="author"
                rules={[

                    {
                        required: true,
                        message: 'please enter your name',
                    },
                ]}
            >
                <Input type="text" />

            </Form.Item>

            <Form.Item
                label="Released"
                name="released"
                rules={[

                    {
                        required: true,
                        message: 'please enter the date',
                    },
                ]}
            >
                <Input type="text" />

            </Form.Item>

            <Form.Item
                name='commentaire'
                label="Commentaire"
            >

                <Input.TextArea />
            </Form.Item>


            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button type="primary" htmlType="submit">
                    Add
                </Button>
            </Form.Item>
        </Form>

    )
}

