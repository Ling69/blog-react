import { Card } from "antd";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";


export function Post({ post }) {

    const selected = useSelector(state => state.post.selected);
    const link = '/post/'+post.id

    return (

        <Card
            style={{ backgroundColor: selected.includes(post) }}
            hoverable
            // cover={<img alt={post.title} src={'process.env.REACT_APP_SERVER_URL+ ' + post.image} />}
            cover={<img alt={post.title} src={post.image.startsWith('http')? post.image:process.env.REACT_APP_SERVER_URL+post.image} />}

            onClick={()=> <Redirect push to={link}/>}
        >
            <Card.Meta title={post.title} description={post.content} />

        </Card>


    )
}