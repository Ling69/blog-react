import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    list: [],
    selected: []
};

export const postSlice = createSlice({
    name: "post",
    initialState,
    reducers: {
        setPosts(state, { payload }) {
            state.list = payload;
        },
        addPost(state, { payload }) {

            state.list.push(payload)

        },
        removePost(state, { payload }) {
            state.list.filter(post => post.id = payload)
        }


    }
});

export const { addPost, setPosts, removePost } = postSlice.actions;

export default postSlice.reducer;

export const fetchPosts = () => async (dispatch) => {

    try {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+ '/api/post');
        dispatch(setPosts(response.data));
    } catch (error) {
        console.log(error);
    }
}


export const uploadPost = ({ title, content, picture, author, released, commentaire }) => async (dispatch) => {


    try {

        const formData = new FormData();
        formData.append("title", title);
        formData.append("content", content);
        formData.append("picture", picture);
        formData.append("author", author);
        formData.append("released", released);
        formData.append("commentaire", commentaire);

        const response = await axios.post(process.env.REACT_APP_SERVER_URL+ '/api/post', formData);
        dispatch(addPost(response.data))
        // console.log(addPost(response.data));
    } catch (error) {
        console.log(error);

    }

}


export const deletePost = (id) => async (dispatch) => {
    try {
        await axios.delete( process.env.REACT_APP_SERVER_URL+ '/api/post/' + id)
        dispatch(removePost(id))
    } catch (error) {
        console.log(error);
    }
}

//https://blogling.herokuapp.com/api/post